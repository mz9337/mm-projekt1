import javax.swing.*;
import javax.swing.text.*;
import javax.swing.event.*;
import java.awt.event.*;
import java.util.*;

import java.io.*;

public class Besedilo extends JPanel{
	private static int width = 800;
	private static int height = 600;
	private static String title = "Urejevalnik Besedila";

	private long lastTime = 0;
	private char lastChar = Character.MIN_VALUE;

	private long[][] matrikaCasov = new long[29][29];
	private long[][] matrikaPonovitev = new long[29][29];
	private long[][] matrikaSestevkov = new long[29][29];

	private JComboBox<String> seznamUporabnikov;
	private JTextField dodajUporabnikaInput;
	private JTextArea textArea;

	private List<Uporabnik> uporabniki;
	private Uporabnik trenutniUporabnik = null;

	public Besedilo(){
		setLayout(null);

		uporabniki = new ArrayList<Uporabnik>();

		textArea = new JTextArea();
		textArea.addKeyListener(new Tipkovnica());
		textArea.setEditable(false);
		
		JScrollPane scrollingArea = new JScrollPane(textArea);
		scrollingArea.setSize(400, 400);
		scrollingArea.setLocation(70, 125);
		//scrollingArea.setEditable(false);
		add(scrollingArea);

		JButton izvozi = new JButton("Izvozi");
		izvozi.setSize(80,30);
		izvozi.setLocation(500, 495);
		izvozi.addActionListener(new IzvozGumb());
		add(izvozi);

		JButton izvoziVse = new JButton("Izvozi vse");
		izvoziVse.setSize(110,30);
		izvoziVse.setLocation(590, 495);
		izvoziVse.addActionListener(new IzvozVseGumb());
		add(izvoziVse);

		//String[] test = {"neki", "nekdo"};

		seznamUporabnikov = new JComboBox<String>();
		seznamUporabnikov.setSize(150,30);
		seznamUporabnikov.setLocation(500, 125);
		seznamUporabnikov.addActionListener(new IzberiUporabnikaSeznam());
		add(seznamUporabnikov);

		dodajUporabnikaInput = new JTextField(20);
		dodajUporabnikaInput.setSize(75,30);
		dodajUporabnikaInput.setLocation(500, 165);
		add(dodajUporabnikaInput);

		JButton dodajUporabnikaGumb = new JButton("Dodaj uporabnika");
		dodajUporabnikaGumb.setSize(150,30);
		dodajUporabnikaGumb.setLocation(585, 165);
		dodajUporabnikaGumb.addActionListener(new DodajUporabnikaGumb());
		add(dodajUporabnikaGumb);

		Okno okno = new Okno(width, height, title, this);
	}

	public static void main(String[] args){
		new Besedilo();
	}

	public int indeks(char c){
		int indeks = (int)c;

		//System.out.println(indeks);

		if(indeks == 32){
			return 26;
		}

		if(indeks == 44){
			return 27;
		}

		if(indeks == 46){
			return 28;
		}

		if(indeks > 96){
			indeks -= 97;
			return indeks;
		}
		if(indeks > 64){
			indeks -= 65;
			return indeks;
		}

		return indeks;
	}

	public void izpisiMatriko(){
		for(int i = 0; i < matrikaCasov.length; i++){
			for(int j = 0; j < matrikaCasov[i].length; j++){
				//System.out.print(matrikaCasov[i][j]+ " ");
				System.out.printf("%5d", matrikaCasov[i][j]);
			}
			System.out.println();
		}
	}

	class Tipkovnica extends KeyAdapter {
		public void keyTyped(KeyEvent e){
			long curTime = System.currentTimeMillis();
			
			int id = e.getID();
	        if (id == KeyEvent.KEY_TYPED) {
	            char c = e.getKeyChar();

	           	if((int)c == 19){
	           		izpisiMatriko();
	           	}

	            if(lastChar == Character.MIN_VALUE){
	            	lastChar = c;
	            	lastTime = curTime;
	            }else{
	            	long time = curTime - lastTime;

	            	int lastIndeks = indeks(lastChar);
	            	int curIndeks = indeks(c);

	            	matrikaSestevkov[lastIndeks][curIndeks] += time;
	            	matrikaSestevkov[curIndeks][lastIndeks] += time;

	            	matrikaPonovitev[lastIndeks][curIndeks] += 1;
	            	matrikaPonovitev[curIndeks][lastIndeks] += 1;

	            	matrikaCasov[lastIndeks][curIndeks] = matrikaSestevkov[lastIndeks][curIndeks]/matrikaPonovitev[lastIndeks][curIndeks];
	            	matrikaCasov[curIndeks][lastIndeks] = matrikaSestevkov[curIndeks][lastIndeks]/matrikaPonovitev[curIndeks][lastIndeks];

	            	System.out.println(lastChar+" to "+c+" = "+time);

	            	lastTime = curTime;
	            	lastChar = c;
	            }
	        } 	        
		}
	}

	class IzvozGumb implements ActionListener{
		public void actionPerformed(ActionEvent e){

			try{
		   		PrintWriter pw = new PrintWriter(new FileWriter("rezultati/"+trenutniUporabnik.getIme()+".txt"));

		   		for(int i = 0; i < matrikaCasov.length; i++){
					for(int j = 0; j < matrikaCasov[i].length; j++){
						pw.printf("%5d", matrikaCasov[i][j]);
					}
					pw.println("");
				}

				pw.close();
			}catch(IOException exc){
				System.out.println(exc);
			}
	    }
	}

	class IzvozVseGumb implements ActionListener{
		public void actionPerformed(ActionEvent e){

			trenutniUporabnik.setMatrikaCasov(matrikaCasov);
			trenutniUporabnik.setMatrikaPonovitev(matrikaPonovitev);
			trenutniUporabnik.setMatrikaSestevkov(matrikaSestevkov);

			try{
		   		PrintWriter pw = new PrintWriter(new FileWriter("rezultati/matrikaVseh.txt"));

		   		long[][] trenutnaMatrikaCasov = null;

		   		for(int i = 0; i < matrikaCasov.length; i++){
		   			for(Uporabnik u: uporabniki){

		   				trenutnaMatrikaCasov = u.getMatrikaCasov();
						for(int j = 0; j < trenutnaMatrikaCasov[i].length; j++){
							pw.printf("%5d", trenutnaMatrikaCasov[i][j]);
						}
					}
					pw.println("");
				}

				pw.close();
			}catch(IOException exc){
				System.out.println(exc);
			}
	    }
	}

	class IzberiUporabnikaSeznam implements ActionListener{
		public void actionPerformed(ActionEvent e){

			JComboBox cb = (JComboBox)e.getSource();
        	String uporabnikIme = (String)cb.getSelectedItem();
			
			//System.out.println(uporabnikIme);

			Uporabnik uporabnik = null;

			for(Uporabnik u: uporabniki){
				if(u.getIme().equals(uporabnikIme)){
					uporabnik = u;
				}
			}

			//System.out.println(uporabnik.getIme());

			if(trenutniUporabnik==null){
				textArea.setEditable(true);
			}else{
				//System.out.println(trenutniUporabnik.getIme());
				trenutniUporabnik.setMatrikaCasov(matrikaCasov);
				trenutniUporabnik.setMatrikaPonovitev(matrikaPonovitev);
				trenutniUporabnik.setMatrikaSestevkov(matrikaSestevkov);

				matrikaCasov = uporabnik.getMatrikaCasov();
				matrikaPonovitev = uporabnik.getMatrikaPonovitev();
				matrikaSestevkov = uporabnik.getMatrikaSestevkov();
			}

			lastChar = Character.MIN_VALUE;
			trenutniUporabnik = uporabnik;
			textArea.setText("");
	    }
	}

	class DodajUporabnikaGumb implements ActionListener{
		public void actionPerformed(ActionEvent e){
			String novUporabnik = dodajUporabnikaInput.getText();
			//System.out.println(novUporabnik);

			if(novUporabnik.length()>0){
				Uporabnik u = new Uporabnik(novUporabnik);
				uporabniki.add(u);

				seznamUporabnikov.addItem(novUporabnik);
				dodajUporabnikaInput.setText("");
			}
	    }
	}
}

class Okno extends JFrame{
	public Okno(int width, int height, String title, JPanel panel){
		this.setSize(width, height);
		this.setTitle(title);
		this.setResizable(false);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.add(panel);
		this.setVisible(true);
	}
}