public class Uporabnik{
	private String ime;

	private long[][] matrikaCasov;
	private long[][] matrikaPonovitev;
	private long[][] matrikaSestevkov;

	public Uporabnik(String ime){
		this.ime = ime;

		this.matrikaCasov = new long[29][29];
		this.matrikaPonovitev = new long[29][29];
		this.matrikaSestevkov =  new long[29][29];
	}

	public String getIme(){
		return this.ime;
	}

	public long[][] getMatrikaCasov(){
		return this.matrikaCasov;
	}

	public long[][] getMatrikaPonovitev(){
		return this.matrikaPonovitev;
	}

	public long[][] getMatrikaSestevkov(){
		return this.matrikaSestevkov;
	}

	public void setIme(String ime){
		this.ime = ime;
	}

	public void setMatrikaCasov(long[][] matrikaCasov){
		this.matrikaCasov = matrikaCasov;
	}

	public void setMatrikaPonovitev(long[][] matrikaPonovitev){
		this.matrikaPonovitev = matrikaPonovitev;
	}

	public void setMatrikaSestevkov(long[][] matrikaSestevkov){
		this.matrikaSestevkov = matrikaSestevkov;
	}
}